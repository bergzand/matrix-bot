// Copyright 2021 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package bot

import (
	"fmt"
	"testing"
)

func testHandler(sender, cmd string, args ...string) *Message {
	return NewTextMessage(fmt.Sprintf("send=%q cmd=%q args=%v", sender, cmd, args))
}

var testCommand1 = &Command{
	Summary:        "one",
	MessageHandler: testHandler,
}

var testCommand2 = &Command{
	Summary:        "two",
	Description:    "two: has a description",
	MessageHandler: testHandler,
	Subcommands: map[string]*Command{
		"subcommand": testCommand1,
	},
}

var commandExecuteTests = []struct {
	Command     *Command
	Sender, Cmd string
	Args        []string
	Out         string
}{
	{
		Command: testCommand1,
		Sender:  "test suite",
		Cmd:     "test",
		Args:    []string{"arg1"},
		Out:     `send="test suite" cmd="test" args=[arg1]`,
	},
	{
		Command: testCommand2,
		Sender:  "test suite",
		Cmd:     "test",
		Args:    []string{"subcommand", "arg1"},
		Out:     `send="test suite" cmd="subcommand" args=[arg1]`,
	},
	{
		Command: &Command{},
		Sender:  "test suite",
		Cmd:     "invalid",
		Args:    []string{"command"},
		Out:     `invalid command: "invalid"`,
	},
}

func TestCommand_Execute(t *testing.T) {
	for _, test := range commandExecuteTests {
		m := test.Command.Execute(test.Sender, test.Cmd, test.Args...)
		if m.Body != test.Out {
			t.Errorf("Incorrect result: expected %q, got %q", test.Out, m.Body)
		}
	}
}

var commandGetCommandTests = []struct {
	Args    []string
	In, Out *Command
}{
	{Args: nil, In: testCommand1, Out: testCommand1},
	{Args: []string{"subcommand"}, In: testCommand2, Out: testCommand1},
}

func TestCommand_GetCommand(t *testing.T) {
	for _, test := range commandGetCommandTests {
		c := test.In.GetCommand("", test.Args...)
		if c != test.Out {
			t.Errorf("Incorrect result: expected %#v, got %#v", test.Out, c)
		}
	}
}

var commandHelpTest = []struct {
	Cmd *Command
	Out string
}{
	{
		Cmd: testCommand1,
		Out: "one",
	},
	{
		Cmd: testCommand2,
		Out: "two: has a description",
	},
}

func TestCommand_Help(t *testing.T) {
	for _, test := range commandHelpTest {
		s := test.Cmd.Help()
		if s != test.Out {
			t.Errorf("Incorrect result: expected %q, got %q", test.Out, s)
		}
	}
}
