package bot

import "testing"

var helpTests = []struct {
	Args []string
	Out  string
}{
	{
		Args: []string{},
		Out: "- `help`: Shows help for a command.\n" +
			"- `test1`: one\n" +
			"- `test2`: two\n" +
			"- `test2 subcommand `: one",
	},
	{
		Args: []string{"invalid"},
		Out:  "- `help`: Shows help for a command.\n- `test1`: one\n- `test2`: two\n- `test2 subcommand `: one",
	},
	{
		Args: []string{"test1"},
		Out:  "one",
	},
	{
		Args: []string{"test1", "nonexistant"},
		Out:  `one`,
	},
	{
		Args: []string{"test2"},
		Out:  "two: has a description\n\n- `subcommand`: one",
	},
	{
		Args: []string{"test2", "subcommand"},
		Out:  "one",
	},
	{
		Args: []string{"test2", "subcommand", "nonexistant"},
		Out:  "one",
	},
}

func TestClient_helpHandler(t *testing.T) {
	c, err := NewClient("", "", "", nil)
	if err != nil {
		t.Fatalf("Error creating test client: %s", err)
	}

	c.SetCommand("test1", testCommand1)
	c.SetCommand("test2", testCommand2)

	for _, test := range helpTests {
		m := c.helpHandler("", "help", test.Args...)
		if m.Body != test.Out {
			t.Errorf("Incorrect help message: expected %q, got %q", test.Out, m.Body)
		}
	}
}
