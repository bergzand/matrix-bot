package bot

import (
	"fmt"
	"io/ioutil"
	"strings"
	"testing"
)

var messageEventID = "$YUwRidLecu:example.com"

func roomTest(t *testing.T, send func(*Room) (string, error), expBody string) {
	t.Helper()

	c, err := NewClient("", "", "",
		&ClientConfig{CommandPrefixes: []string{"!"}})
	if err != nil {
		t.Fatalf("Error creating test client: %s", err)
	}

	roundTripper := &TestRoundTripper{Responses: map[string]string{
		"/_matrix/client/r0/rooms/%21testID/send/m.room.message/.*": fmt.Sprintf(`{"event_id":%q}`, messageEventID),
		"/_matrix/client/r0/join/%21testID":                         `{"room_id": "!testID"}`,
	}}

	c.Client.Client.Transport = roundTripper

	id, err := send(c.NewRoom("!testID"))
	if err != nil {
		t.Errorf("Error sending message: %s", err)
	}

	if id != messageEventID {
		t.Errorf("Unexpected event ID: expected %q, got %q", messageEventID, id)
	}

	var data []byte
	if roundTripper.Requests[0].Body != nil {
		data, _ = ioutil.ReadAll(roundTripper.Requests[0].Body)
	}

	got := strings.TrimSpace(string(data))
	if got != expBody {
		t.Errorf("Unexpected body: expected \n%q, got \n%q", expBody, got)
	}
}

func TestRoom_SendMarkdown(t *testing.T) {
	roomTest(t, func(r *Room) (string, error) { return r.SendMarkdown("*test*") },
		`{"msgtype":"m.notice","body":"*test*",`+
			`"formatted_body":"\u003cp\u003e\u003cem\u003etest\u003c/em\u003e\u003c/p\u003e\n",`+
			`"format":"org.matrix.custom.html"}`)
}

func TestRoom_SendText(t *testing.T) {
	roomTest(t, func(r *Room) (string, error) { return r.SendText("*test*") },
		`{"msgtype":"m.notice","body":"*test*"}`)
}

func TestRoom_SendHTML(t *testing.T) {
	roomTest(t, func(r *Room) (string, error) { return r.SendHTML("*test*", "<b><i>test</b></i>") },
		`{"msgtype":"m.notice","body":"*test*",`+
			`"formatted_body":"\u003cb\u003e\u003ci\u003etest\u003c/b\u003e\u003c/i\u003e",`+
			`"format":"org.matrix.custom.html"}`)
}

func TestRoom_Allowed1(t *testing.T) {
	c, err := NewClient("", "", "", nil)
	if err != nil {
		t.Fatalf("Error creating test client: %s", err)
	}

	if !c.NewRoom("!test").Allowed() {
		t.Errorf("Expected all rooms to be allowed, but was not.")
	}
}

func TestRoom_Allowed2(t *testing.T) {
	tests := map[string]bool{
		"!allowed": true,
		"!denied":  false,
	}

	c, err := NewClient("", "", "", &ClientConfig{
		AllowedRooms: []string{"!allowed"},
	})
	if err != nil {
		t.Fatalf("Error creating test client: %s", err)
	}

	for roomID, allowed := range tests {
		res := c.NewRoom(roomID).Allowed()
		if res != allowed {
			t.Errorf("Incorrect allowed: expected %v, got %v", allowed, res)
		}
	}
}

func TestRoom_Join(t *testing.T) {
	roomTest(t, func(r *Room) (string, error) { return messageEventID, r.Join() }, ``)
}
