module gitlab.com/silkeh/matrix-bot

go 1.17

require (
	github.com/matrix-org/gomatrix v0.0.0-20210324163249-be2af5ef2e16
	github.com/russross/blackfriday/v2 v2.1.0
)
